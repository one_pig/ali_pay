package io.pay.common.consts;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-01-23
 * @description:
 */
public class AlipayConsts {
    public static final String APP_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCIsV9e4n9AJelETvdSltc37Tvnxv9oU3xayYT1SswGkGI5xzHsjERSUcuYtRm4oDVIWQnueFlo8OENef12dH7Mlm9HeiSsWgOZqSqEFZIZhaYUW73xQLhezFqboKriJn6I0gM0H2PG0N+hEP7kNvG2heoawH5YZUqZmJDHyzC52GHNJ1q7YTBQLPaxNphLsdiMP/xwSaTn/SJA1qwA2n2ZjG2TGkMrjz6p5wPWyac1N9TbllFfjPNDIdZQAQuH3CHG4Jlo9lH8uQzgYxXFocvTuwYyyNHLd+hoOdDFVP3sDIGNjpTgCPlktIPKs0y4MYmyWmtkZi48GAAMhcZWdofHAgMBAAECggEAKas4UWpmNB3acBJKXojQ7qGI42zMKvdYLyzDNSsp6s5Uwxwv4dYYdaetks2SG6/qzjUmS+WwC8dyM3N+in+Ur7UkEvvu9gMymcLxzPXuZY1SRV56FnMOQQe2jz55GRt28ypiA5+WAONnaGgP6228XVGRPbp0SWoHRa82Y1FTdzNG4eFYefr/yGDADazXQmIANT7Gq86+VwAN9NN/1f9iobEWei0fRfddNrgeacVPEjcgoh9+bVIyvUS8ip10mrbpaLkWZqpiVSTE0GUwY/hGpVXZRekRik2HTJBSp9DXy+5u2kRw/dDnojZTJwbGVM5OOGFnSu0pCoXR4DheidCNIQKBgQDDzCYqyCR/wk5DmdMBVcFtTBXgqJxHrHoQShcicjkPVc9X4s7m6s3FP4yBxAw/FXyHuAOxKSvbprW7TXO71Y2iftnSx9NfgVd0omMvRLxCTsASexx6lAbvtqyJpytkNu+m3iIavlXdAjyBSepFu5Vo4g51oAH27Mur6+QeBtzHnwKBgQCyuOjckIy0M4yh8M6gbW1f2xMEwiYl4Pz4ocWna6Ho2ydPuGXkgQx5DjqPlKcZc5Wkg9sypDaz0lnIpjaysi9HzQ5Y8aZ1UeaUdb2qxmRJTNkkAQrWwCDc4sNJUfAYgX6doUKpT1G0hl/ZvKdVnpJm5eFm9nABZGEOhVJNiJVu2QKBgBR3CB48oSQBaTDDqTxgfEEyTK8YsI0FSBTfyZ8+FRy3OV7URA/6xtsxEzyVAzqswkFlSG2CAiWJPUGAjPI7FfzACYBwhnVbDYsRVhCgvJSpqoXD/GwLgNYZX9tbb7ufyljEqwfkkRDypORWl3gGbU1zI1PED7CBe6BSnZtN2FG9AoGBAJCjw5lVxvnDymn5CFCfnuf31aGBOvxm5HSX/+2p1wQr8Nm844GVyzFhiZVP+hhICfB14f1PFxJbB9yyYAwo1sjmNFlZYJ0MJm86BsDiBKGp9wYDbfE01qPNRbzd+lDq5DzQGLILhpVyNrpxyn8pcVSjcNMyWJ1XzpDA50XmZwGpAoGAKcuFqRRpUCgL1S2US7CD02CNN50+WTvIkRpzYTnwKkwQd4Svo+e3oL+I5hvqhMgOzaN1/rjuD1Jc+1XQp+LHeXhMKkKpSigI3048bVh8iCigwhQjTXRjtji9moVK5q+Nizz+0lZ1Yx9uoaKiQrhwGe0M73ZBkmzFndl5ccoe9lE=";
    public static final String APP_ID = "2018051660110688";

    public static final String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5UBE78qpy68cBKOTJvckRjVyVTITXJrMkRITStuvYBpJzrztdvG/3WDNHME+tGzWiAsTeJxwkxq2AGdx0O8CEbz8t4+N7+TEDaYCAfm0URXSaDvn+FuT9bUEOCqXmv9AZ87uv4yTVLOXc5puiSDkV80AoA0aYTZPfgwKlY781RvPdpVpIBDsq9Wn34bzYYIyiixFxsO4EcQIdezXv0nPg+CGTgRATWcPZJZVXXX87TRCLrL7Rj2KqRiA26QOR3PQ1fDc9MLc4Ie9tQpAKud50nb/HPN7yJuYFjw44GLiP6DYqzNDhcMvTFiQUilP7tNyD5zhVv9INq002yUofmrCDQIDAQAB";

    public static final String PID = "2088031995700174";

    public static final String CONSTANT_NOTIFY_URL = "http://47.105.68.91:8080/alipay/pay_notify";

    public static final String CHARSET = "utf-8";

    public static final String SIGH_TYPE = "RSA2";

    public static final String FORMAT = "json";

    // https://openapi.alipaydev.com/gateway.do
    // https://openapi.alipay.com/gateway.do
    public static final String GATEWAY_ROOT = "https://openapi.alipay.com/gateway.do";

    public static final String FIELD_NAME_OUT_TRADE_NO = "out_trade_no";

    public static final String FIELD_NAME_SELLER_ID = "seller_id";

    public static final String FIELD_NAME_TOTAL_AMOUNT = "total_amount";

    public static final String FIELD_NAME_SUBJECT = "subject";

    public static final String FIELD_NAME_OUT_REQUEST_NO = "out_request_no";

    public static final String FIELD_NAME_TRADE_NO = "trade_no";

    public static final String FIELD_NAME_NOTIFY_URL = "notify_url";

    public static final String FIELD_NAME_ROYALTY_PARAMETERS = "royalty_parameters";

    public static final String FIELD_NAME_TRANS_OUT = "trans_out";

    public static final String FIELD_NAME_TRANS_IN = "trans_in";

    public static final String FIELD_NAME_AMOUNT = "amount";

    public static final String FIELD_NAME_AMOUNT_PERCENTAGE = "amount_percentage";

    public static final String FIELD_NAME_DESC = "desc";

    public static final String FIELD_NAME_OUT_BIZ_NO = "out_biz_no";

    public static final String FIELD_NAME_PAYEE_TYPE = "payee_type";

    public static final String FIELD_NAME_PAYEE_ACCOUNT = "payee_account";

    public static final String FIELD_NAME_PAYER_SHOW_NAME = "payer_show_name";

    public static final String FIELD_NAME_REMARK = "remark";

    public static final String FIELD_NAME_GRANT_TYPE = "grant_type";

    public static final String FIELD_NAME_CODE = "code";

    public static final String FIELD_NAME_APP_AUTH_TOKEN = "app_auth_token";

    public static final String FIELD_VALUE_GRANT_TYPE = "authorization_code";

    public static final String FIELD_VALUE_PAYEE_TYPE_PID = "ALIPAY_USERID";

    public static final String FIELD_TRADE_STATUS = "trade_status";
    public static final List<String> TRADE_STATUS_SUCCESS = Lists.newArrayList("TRADE_SUCCESS", "TRADE_FINISHED");

    public static final Double PRICE_ZERO = 0.0;
}
