package io.pay.common.consts;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-03-02
 * @description:
 */
public class BizConsts {
    public static final String BIZ_NAME = "云学生活服务";

    public static final String BIZ_NAME_EN = "yunxueservice";
}
