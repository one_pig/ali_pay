package io.pay.common.utils;

import java.text.DecimalFormat;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-03-04
 * @description:
 */
public class NumberUtils {
    public static long RANDOM_RANGE = 100000L;

    public static long generateRandom(){
        return generateRandom(RANDOM_RANGE);
    }

    public static long generateRandom(long range){
        return (long) (Math.random() * range);
    }

    public static double format2digit(Double num){
        return Double.parseDouble(new DecimalFormat("#.00").format(num));
    }
}
