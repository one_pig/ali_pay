package io.pay.common.utils;

import com.google.gson.Gson;

import java.util.Map;

/**
 * Created by laaaa on 2018/12/16.
 */
public class JsonUtils {
    private static Gson gson = new Gson();

    public static String toJsonString(Map<String, Object> map){
        return gson.toJson(map);
    }
    public static String toJsonString(Object obj){
        return gson.toJson(obj);
    }
}
