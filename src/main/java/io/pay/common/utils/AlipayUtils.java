package io.pay.common.utils;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import io.pay.common.consts.AlipayConsts;
import io.pay.common.consts.BizConsts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-02-14
 * @description:
 */
public class AlipayUtils {
    public static final String TRADE_NO_PREFIX = "tradeNo";

    public static final String SETTLE_PREFIX = "settleNo";

    public static final String TRANSFER_PREFIX = "transferNo";

    public static String generateOutTradeNo(){
        return "tradepay" + System.currentTimeMillis()
                + (long) (Math.random() * 10000000L);
    }

    public static String generateServiceNo(String prefix){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        Date date = new Date();
        simpleDateFormat.format(date);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(prefix);
        stringBuilder.append(DateUtils.getCurrentTime(DateUtils.DATE_FORMAT_YYYYMMDDHHMMSS));
        stringBuilder.append(String.valueOf(NumberUtils.generateRandom()));

        return stringBuilder.toString();
    }

    public static String generatePrecreateOrderSubject(){
        return "线上收银台";
    }

    public static String generateSettleDesc(String userInfo){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("来自用户");
        stringBuilder.append(userInfo);
        stringBuilder.append("下单的分账");
        return stringBuilder.toString();
    }

    public static String generateTransferRemark(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("来自");
        stringBuilder.append(BizConsts.BIZ_NAME_EN);
        stringBuilder.append("的转账");
        return stringBuilder.toString();
    }

    public static Boolean checkSign(Map<String, String> params){
        try {
            return AlipaySignature.rsaCheckV1(params, AlipayConsts.ALIPAY_PUBLIC_KEY, AlipayConsts.CHARSET, AlipayConsts.SIGH_TYPE);
        } catch (AlipayApiException e) {
            return false;
        }
    }


    public static Boolean isTradeSuccess(String code){
        return code.equals("TRADE_SUCCESS") || code.equals("TRADE_FINISHED");
    }
}
