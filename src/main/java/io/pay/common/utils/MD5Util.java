package io.pay.common.utils;

import java.security.MessageDigest;

public class MD5Util {
    public static String getUUID() {
        long currentTimeInMs = System.currentTimeMillis();
        long ca = (long)(currentTimeInMs / 600000) * 600000;
        String tenMinutes = DateUtils.parseTime(ca, DateUtils.FORMATTER_MINUTE_PATTERN);
        return encode16("root" + tenMinutes + "Passw0rd");
    }

    public static String encode(String content) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(content.getBytes());
            return getEncode16(digest);
        } catch (Exception e) {

        }
        return null;
    }

    public static String encode16(String content) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(content.getBytes());
            return getEncode16(digest);
        } catch (Exception e) {

        }
        return null;
    }

    public static String encode32(String content) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(content.getBytes());
            return getEncode32(digest);
        } catch (Exception e) {

        }
        return null;
    }


    /**
     * 32位加密
     *
     * @param digest
     * @return
     */
    public static String getEncode32(MessageDigest digest) {
        StringBuilder builder = new StringBuilder();
        for (byte b : digest.digest()) {
            builder.append(Integer.toHexString((b >> 4) & 0xf));
            builder.append(Integer.toHexString(b & 0xf));
        }
        //return builder.toString().toLowerCase(); // 小写
        //return builder.toString().toUpperCase(); // 大写
        // java.lang.String.toUpperCase(Locale locale) 方法将在此字符串中的所有字符为大写的规则给定的Locale.
        //return builder.toString().toUpperCase(Locale.getDefault()); // 大写
        return builder.toString();
    }

    /**
     * 16位加密
     *
     * @param digest
     * @return
     */
    public static String getEncode16(MessageDigest digest) {
        StringBuilder builder = new StringBuilder();
        for (byte b : digest.digest()) {
            builder.append(Integer.toHexString((b >> 4) & 0xf));
            builder.append(Integer.toHexString(b & 0xf));
        }

        // 16位加密，从第9位到25位
        //return builder.substring(8, 24).toString().toUpperCase();
        return builder.substring(8, 24).toString();
    }
}