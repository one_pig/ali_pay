package io.pay.common.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by laaaa on 2018/12/15.
 */
public class ObjectUtils {

    public static final char UNDERLINE = '_';


    /**
     * 简单对象 转换
     * @param o
     * @return
     */
    public static Map<String,String> toMap(Object o){
        Map<String,String> map = new HashMap<>();
        Field[] fields = o.getClass().getDeclaredFields();
        try {
            for (int i = 0;i < fields.length; i++) {
                String name = fields[i].getName();
                Field field = o.getClass().getDeclaredField(name);
                field.setAccessible(true);
                if (null != field) {
                    map.put(name, field.get(o).toString());
                }
            }
        }catch(Exception e){
//            log.error(e.getMessage(),e);
        }
        return map;
    }

    /**
     * 驼峰格式字符串转换为下划线格式字符串
     *
     * @param param
     * @return
     */
    public static String camelToUnderline(String param) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (Character.isUpperCase(c)) {
                sb.append(UNDERLINE);
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    /**
     * 下划线格式字符串转换为驼峰格式字符串
     *
     * @param param
     * @return
     */
    public static String underlineToCamel(String param) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (c == UNDERLINE) {
                if (++i < len) {
                    sb.append(Character.toUpperCase(param.charAt(i)));
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

}
