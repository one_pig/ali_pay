package io.pay.common.utils;

import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2016年10月27日 下午9:59:27
 */
public class ResultResponse extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public ResultResponse() {
		put("code", 0);
		put("msg", "success");
	}
	
	public static ResultResponse error() {
		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, "未知异常，请联系管理员");
	}
	
	public static ResultResponse error(String msg) {
		return error(HttpStatus.SC_INTERNAL_SERVER_ERROR, msg);
	}
	
	public static ResultResponse error(int code, String msg) {
		ResultResponse resultResponse = new ResultResponse();
		resultResponse.put("code", code);
		resultResponse.put("msg", msg);
		return resultResponse;
	}

	public static ResultResponse ok(String msg) {
		ResultResponse resultResponse = new ResultResponse();
		resultResponse.put("msg", msg);
		return resultResponse;
	}
	
	public static ResultResponse ok(Map<String, Object> map) {
		ResultResponse resultResponse = new ResultResponse();
		resultResponse.putAll(map);
		return resultResponse;
	}
	
	public static ResultResponse ok() {
		return new ResultResponse();
	}

	public ResultResponse put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
