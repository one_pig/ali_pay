package io.pay.modules.app.form;

/**
 * Created by laaaa on 2018/12/16.
 */
public class AlipayCreateParam {
    private Double money;

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }
}
