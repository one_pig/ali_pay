package io.pay.modules.app.model;

/**
 * Created by laaaa on 2018/12/16.
 */
public class AlipaySellerInfoDO {
    private int id;
    private String userId;
    private String sellerDescription;
    private double sellerRate;
    private int sellerDailyGmv;
    private int sellerQuota;
    private String appAuthToken;
    private String appRefreshToken;
    private String authAppId;
    private String expiresIn;
    private String reExpiresIn;

    private long ctime;
    private long utime;
    private int state;
    private String gmtCreate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSellerDescription() {
        return sellerDescription;
    }

    public void setSellerDescription(String sellerDescription) {
        this.sellerDescription = sellerDescription;
    }

    public double getSellerRate() {
        return sellerRate;
    }

    public void setSellerRate(double sellerRate) {
        this.sellerRate = sellerRate;
    }

    public int getSellerDailyGmv() {
        return sellerDailyGmv;
    }

    public void setSellerDailyGmv(int sellerDailyGmv) {
        this.sellerDailyGmv = sellerDailyGmv;
    }

    public int getSellerQuota() {
        return sellerQuota;
    }

    public void setSellerQuota(int sellerQuota) {
        this.sellerQuota = sellerQuota;
    }

    public String getAppAuthToken() {
        return appAuthToken;
    }

    public void setAppAuthToken(String appAuthToken) {
        this.appAuthToken = appAuthToken;
    }

    public String getAppRefreshToken() {
        return appRefreshToken;
    }

    public void setAppRefreshToken(String appRefreshToken) {
        this.appRefreshToken = appRefreshToken;
    }

    public String getAuthAppId() {
        return authAppId;
    }

    public void setAuthAppId(String authAppId) {
        this.authAppId = authAppId;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getReExpiresIn() {
        return reExpiresIn;
    }

    public void setReExpiresIn(String reExpiresIn) {
        this.reExpiresIn = reExpiresIn;
    }

    public long getCtime() {
        return ctime;
    }

    public void setCtime(long ctime) {
        this.ctime = ctime;
    }

    public long getUtime() {
        return utime;
    }

    public void setUtime(long utime) {
        this.utime = utime;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }
}
