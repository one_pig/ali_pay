package io.pay.modules.app.model;

/**
 * Created by laaaa on 2018/12/16.
 */
public class AlipaySellerInfoVO {
    private int id;
    private String userId;
    private String sellerDescription;
    private String sellerRate;
    private String sellerDailyGmv;
    private String sellerQuota;
    private String state;
    private String gmtCreate;
    private String qrCode;
    private String qrCodePath;
    private String operation;
    private String nickName;
    private String cityName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSellerDescription() {
        return sellerDescription;
    }

    public void setSellerDescription(String sellerDescription) {
        this.sellerDescription = sellerDescription;
    }

    public String getSellerRate() {
        return sellerRate;
    }

    public void setSellerRate(String sellerRate) {
        this.sellerRate = sellerRate;
    }

    public String getSellerDailyGmv() {
        return sellerDailyGmv;
    }

    public void setSellerDailyGmv(String sellerDailyGmv) {
        this.sellerDailyGmv = sellerDailyGmv;
    }

    public String getSellerQuota() {
        return sellerQuota;
    }

    public void setSellerQuota(String sellerQuota) {
        this.sellerQuota = sellerQuota;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getQrCodePath() {
        return qrCodePath;
    }

    public void setQrCodePath(String qrCodePath) {
        this.qrCodePath = qrCodePath;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
