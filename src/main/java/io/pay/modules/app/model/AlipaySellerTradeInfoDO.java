package io.pay.modules.app.model;

/**
 * Created by laaaa on 2018/12/16.
 */
public class AlipaySellerTradeInfoDO {
    private String userId;
    //码主佣金率
    private double sellerRate;
    //码主单笔限额
    private int sellerQuota;
    //码主日限额
    private double dailyGmv;
    //码主日可用额度
    private double availableGmv;

    private String gmtCreate;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getSellerRate() {
        return sellerRate;
    }

    public void setSellerRate(double sellerRate) {
        this.sellerRate = sellerRate;
    }

    public int getSellerQuota() {
        return sellerQuota;
    }

    public void setSellerQuota(int sellerQuota) {
        this.sellerQuota = sellerQuota;
    }

    public double getDailyGmv() {
        return dailyGmv;
    }

    public void setDailyGmv(double dailyGmv) {
        this.dailyGmv = dailyGmv;
    }

    public double getAvailableGmv() {
        return availableGmv;
    }

    public void setAvailableGmv(double availableGmv) {
        this.availableGmv = availableGmv;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }
}
