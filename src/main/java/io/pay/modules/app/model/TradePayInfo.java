package io.pay.modules.app.model;

import com.alipay.api.response.AlipayTradeQueryResponse;

public class TradePayInfo {
    //private String
    String tradeNo;
    String outTradeNo;
    String buyerLogonId;
    String tradeStatus;
    String totalAmount;
    String buyerUserId;

    public String getBuyerUserId() {
        return buyerUserId;
    }

    public void setBuyerUserId(String buyerUserId) {
        this.buyerUserId = buyerUserId;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getBuyerLogonId() {
        return buyerLogonId;
    }

    public void setBuyerLogonId(String buyerLogonId) {
        this.buyerLogonId = buyerLogonId;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }



    public TradePayInfo(AlipayTradeQueryResponse alipayTradeQueryResponse) {

        //alipayTradeQueryResponse.getTotalAmount();

//        String tradeNo;
//        String outTradeNo;
//        String buyerLogonId;
//        String tradeStatus;
//        String totalAmount;
//        String buyerUserId;
        AlipayTradeQueryResponse r = alipayTradeQueryResponse;
        tradeNo = r.getTradeNo();
        outTradeNo = r.getOutTradeNo();
        buyerLogonId = r.getBuyerLogonId();
        tradeStatus = r.getTradeStatus();
        totalAmount = r.getTotalAmount();
        buyerUserId = r.getBuyerUserId();

    }
}
