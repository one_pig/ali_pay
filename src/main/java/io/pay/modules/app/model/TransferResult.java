package io.pay.modules.app.model;


public class TransferResult {
    public String outBizNo;
    public String orderId;
    public String payDate;

    public TransferResult(String outBizNo, String orderId, String payDate) {
        this.outBizNo = outBizNo;
        this.orderId = orderId;
        this.payDate = payDate;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOutBizNo() {
        return outBizNo;
    }

    public void setOutBizNo(String outBizNo) {
        this.outBizNo = outBizNo;
    }
}
