package io.pay.modules.app.model;


public class SettleResult {
    public String tradeNo;
    public SettleResult(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }
}
