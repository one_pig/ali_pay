package io.pay.modules.app.model;


public class PreOrder {
    String outTradeNo;
    String qrCode;

    public PreOrder(String outTradeNo, String qrCode) {
        this.outTradeNo = outTradeNo;
        this.qrCode = qrCode;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}
