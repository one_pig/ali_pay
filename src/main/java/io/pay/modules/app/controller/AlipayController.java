package io.pay.modules.app.controller;

import com.alibaba.fastjson.JSON;
import com.alipay.api.response.*;
import io.pay.common.consts.AlipayConsts;
import io.pay.common.consts.BizConsts;
import io.pay.common.utils.*;
import io.pay.modules.app.form.AliRecallParam;
import io.pay.modules.app.form.AlipayCreateParam;
import io.pay.modules.app.model.AlipaySellerInfoDO;
import io.pay.modules.app.model.PreOrder;
import io.pay.modules.app.model.PreOrderInfoDO;
import io.pay.modules.app.service.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by laaaa on 2018/12/16.
 */
@RestController
@RequestMapping("/alipay")
public class AlipayController {
    private static final Logger LOG = LoggerFactory.getLogger(AlipayController.class);

    @Autowired
    TradeService tradeService;

    @Autowired
    AlipayTradeOrderSettleService alipayTradeOrderSettleService;

    @Autowired
    AlipayFundService alipayFundTransToaccountTransfer;

    @Autowired
    AlipayAuthService alipayAuthService;

    @Autowired
    OrderInfoDBService orderInfoDBService;

    @Autowired
    DatabaseService databaseService;

    @Autowired
    ManagerService managerService;

    /**
     * 支付回调
     *
     * @param request
     */
    @RequestMapping(value = "/pay_notify", method = RequestMethod.POST)
    @ResponseBody
    public String processOrder(HttpServletRequest request) {
        LOG.info("<== 支付回调 start ==>");
        Map<String,String> params = convertRequestParamsToMap(request);
        Map<String,String> paramsRaw = convertRequestParamsToMapRaw(request);
        AliRecallParam aliRecallParam = buildAlipayNotifyParam(params);
        LOG.info("<== 支付回调 params:{} ==>", JsonUtils.toJsonString(aliRecallParam));
        // 判断交易是否合法, 合法则继续
        if (isTradeLegal(aliRecallParam,paramsRaw)) {
            try {
                LOG.info("Trade is Legal");
                String sellerId = aliRecallParam.getSellerId();
                String tradeNo = aliRecallParam.getTradeNo();
                String buyerId = aliRecallParam.getBuyerId();
                String buyerLogonId = aliRecallParam.getBuyerLogonId();
                String outTradeNo = aliRecallParam.getOutTradeNo();

                LOG.info(sellerId + ":" + tradeNo + ":" + buyerId + ":" + buyerLogonId);
                //回调回来订单数据插入数据库
                orderInfoDBService.insertOrderInfo(aliRecallParam);
                LOG.info("insertOrderInfo done. outTradeNo:{}", outTradeNo);
                AlipaySellerInfoDO sellerInfo = databaseService.selectSellerInfoBySellerId(sellerId);
                LOG.info("selectSellerInfoBySellerId done. sellerInfoDO:" + JsonUtils.toJsonString(sellerInfo));
                double sellerRate = sellerInfo.getSellerRate();

                Double amount = NumberUtils.format2digit(aliRecallParam.getTotalAmount());
                Double settleAmount = NumberUtils.format2digit(amount * (100d - sellerRate) / 100d);
                AlipayTradeOrderSettleResponse settleResponse = alipayTradeOrderSettleService.orderSettle(
                        AlipayUtils.generateServiceNo(AlipayUtils.SETTLE_PREFIX),
                        outTradeNo, tradeNo, sellerId, AlipayConsts.PID,
                        settleAmount, 100, AlipayUtils.generateSettleDesc(buyerLogonId),
                        sellerInfo.getAppAuthToken());
                LOG.info("orderSettle done. AlipayTradeOrderSettleResponse = " + JsonUtils.toJsonString(settleResponse));

                // 判断分账是否成功, 成功则继续
                if (settleResponse.isSuccess()) {

                    // 转给用户，一开始先注销
                    AlipayFundTransToaccountTransferResponse transferToBuyerResponse =
                            alipayFundTransToaccountTransfer.alipayFundTransToaccount(outTradeNo,
                                    AlipayUtils.generateServiceNo(AlipayUtils.TRANSFER_PREFIX),
                                    AlipayConsts.FIELD_VALUE_PAYEE_TYPE_PID,
                                    buyerId,
                                    NumberUtils.format2digit(amount * 0.8),
                                    BizConsts.BIZ_NAME_EN,
                                    AlipayUtils.generateTransferRemark());
                    LOG.info("transferToUser done. transferToBuyerResponse = " + JsonUtils.toJsonString(transferToBuyerResponse));
                }
                LOG.info("<== 支付回调 处理SUCCESS ==>");
            } catch (Exception e) {
                LOG.error("支付宝回调业务处理报错,params:", e);
            }
        }
        LOG.info("<== 支付回调 end ==>");
        return "SUCCESS";
    }

    /**
     * 预创建订单
     *
     * @param alipayCreateParam
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/precreate",method = RequestMethod.POST )
    public ResponseEntity<String> preCreate(@RequestBody AlipayCreateParam alipayCreateParam) throws Exception {
        LOG.info("<== 预创建 start ==>");

        double money = alipayCreateParam.getMoney();
        //为空的情况，如果调阿里接口会变成默认转账给包租婆账户
        String sellerId = databaseService.selectSellerIdByMoney(money);
        AlipaySellerInfoDO sellerInfo = databaseService.selectSellerInfoBySellerId(sellerId);
        LOG.info("selectSeller done. sellerId={}", sellerId);
        if (StringUtils.isEmpty(sellerId)) {
            return JsonHelper.jsonpEntity("没有选择到合适的订单, 联系阿鹏生活服务", null);
        } else {
            AlipayTradePrecreateResponse response =
                    tradeService.precreateTrade(AlipayUtils.generateServiceNo(AlipayUtils.TRADE_NO_PREFIX),
                            sellerId, money, AlipayUtils.generatePrecreateOrderSubject(),
                            AlipayConsts.CONSTANT_NOTIFY_URL, sellerInfo.getAppAuthToken());

            if (response.isSuccess()) {
                PreOrder preOrder = new PreOrder(response.getOutTradeNo(), response.getQrCode());
                LOG.info("precreateTrade done. AlipayTradePrecreateResponse={}", JsonUtils.toJsonString(response));
                LOG.info("<== 预创建 end ==>");
                return JsonHelper.jsonpEntity(preOrder, null);
            } else {
                return JsonHelper.jsonpEntity("没有选择到合适的订单, 联系阿鹏生活服务", null);
            }
        }
    }


    @RequestMapping(value = "/validate")
    @ResponseBody
    public String validateUserOrder(HttpServletRequest request, String sellerId, HttpServletResponse servletResponse) throws Exception {
        LOG.info("<== 预创建 start ==>");

        AlipaySellerInfoDO sellerInfo = databaseService.selectSellerInfoBySellerId(sellerId);
        LOG.info("selectSeller done. sellerId={}", sellerId);
        // 为空的情况，如果调阿里接口会变成默认转账给包租婆账户
        if (StringUtils.isEmpty(sellerId)) {
            return "没有选择到合适的订单, 联系阿鹏生活服务";
        } else {
            AlipayTradePrecreateResponse response = tradeService.precreateTrade(AlipayUtils.generateServiceNo(AlipayUtils.TRADE_NO_PREFIX), sellerId, 1.0, AlipayUtils.generatePrecreateOrderSubject(), AlipayConsts.CONSTANT_NOTIFY_URL, sellerInfo.getAppAuthToken());

            if (response.isSuccess()) {
                LOG.info("precreateTrade done. AlipayTradePrecreateResponse={}" , JsonUtils.toJsonString(response));
                LOG.info("<== 预创建 end ==>");
                servletResponse.sendRedirect(response.getQrCode());
            } else {
                return "没有选择到合适的订单, 联系阿鹏生活服务";
            }
        }
        return "SUCCESS";
    }

    /**
     * 码主授权
     *
     * @param request
     * @param response
     */
    @RequestMapping("/notify_url")
    public void notify(HttpServletRequest request, HttpServletResponse response) {
        LOG.info("<== 码主授权 start ==>");
        String appAuthCode = request.getParameter("app_auth_code");
        LOG.info("appAuthCode:" + appAuthCode);

        AlipayOpenAuthTokenAppResponse alipayOpenAuthTokenAppResponse =
                alipayAuthService.alipayOpenAuthTokenApp(AlipayConsts.FIELD_VALUE_GRANT_TYPE, appAuthCode);

        LOG.info("alipay_open_auth_token_app_response:{}", JsonUtils.toJsonString(alipayOpenAuthTokenAppResponse));
        LOG.info("<== 码主授权 end ==>");
    }

    /**
     * 码主授权
     *
     * @param request
     * @param response
     */
    @RequestMapping("/notify_url_user_info")
    public void notifyUserInfo(HttpServletRequest request, HttpServletResponse response) {
        LOG.info("<== 用户信息 start ==>");
        String authCode = request.getParameter("auth_code");
        LOG.info("authCode:{}" ,authCode);

        AlipaySystemOauthTokenResponse systemOauthTokenResponse = alipayAuthService.alipaySystemOauthToken(authCode);
        LOG.info("systemOauthTokenResponse={}", JsonUtils.toJsonString(systemOauthTokenResponse));
        String accessToken = systemOauthTokenResponse.getAccessToken();
        AlipayUserInfoShareResponse userInfoShareResponse = alipayAuthService.alipayGetUserInfo(accessToken);
        LOG.info("userInfoShareResponse={}", JsonUtils.toJsonString(userInfoShareResponse));

        LOG.info("<== 用户信息 end ==>");
    }

    @RequestMapping(value = "notify")
    public ResponseEntity<String> notify(
            @RequestParam(value = "trade_no") String tradeNo,
            @RequestParam(value = "buyer_email") String buyerEmail,
            @RequestParam(value = "total_fee") String totalFee) {
        tradeService.settle("2018040112345", tradeNo, Double.valueOf(totalFee));

        Double userGetMoney = Double.valueOf(totalFee) * 0.9 - 1;

        tradeService.transfer("201804234239", "ALIPAY_LOGONID", buyerEmail, userGetMoney.toString());

        LOG.info("分账完毕，转账完毕");
        return JsonHelper.jsonpEntity("Fenzhang wanbi, transfer complete", null);

    }

    @RequestMapping("/queryAuth")
    public String queryAth(HttpServletRequest request, HttpServletResponse response) {
        String appAuthToken = request.getParameter("appAuthToken");
        AlipayOpenAuthTokenAppQueryResponse alipayOpenAuthTokenAppQueryResponse = alipayAuthService.alipayOpenAuthTokenQuery(appAuthToken);
        LOG.info("queryAuthInfo =" + JsonUtils.toJsonString(alipayOpenAuthTokenAppQueryResponse));
        return JsonUtils.toJsonString(alipayOpenAuthTokenAppQueryResponse);
    }

    private Boolean isTradeLegal(AliRecallParam aliRecallParam,Map<String,String> paramsRaw) {
        LOG.info("IsTradeLegal start.");
        Map<String, String> params = ObjectUtils.toMap(aliRecallParam);
        Boolean isFromAlipay = AlipayUtils.checkSign(paramsRaw);
        Boolean isTradeSuccess = false;
        if (null != aliRecallParam.getTradeStatus()) {
            if (AlipayConsts.TRADE_STATUS_SUCCESS.contains(aliRecallParam.getTradeStatus())) {
                isTradeSuccess = true;
            }
        }
        String outTradeNo = aliRecallParam.getOutTradeNo();
        if (null == aliRecallParam.getOutTradeNo()) {
            outTradeNo = "";
        }
        LOG.info("outTradeNo: {}", outTradeNo);
        Boolean isTradeExist = false;
        if (StringUtils.isNotBlank(outTradeNo)) {
            PreOrderInfoDO preOrderInfoDO = managerService.selectByOutTradeNo(outTradeNo);
            if (preOrderInfoDO != null && (StringUtils.isNotEmpty(preOrderInfoDO.getSellerId()))) {
                isTradeExist = true;
            }
        }
        LOG.info("isFromAlipay: {}", isFromAlipay);
        LOG.info("isTradeSuccess {}", isTradeSuccess);
        LOG.info("isTradeExist {}", isTradeExist);
        return isFromAlipay && isTradeSuccess && isTradeExist;
    }

    /**
     * 将request中的参数转换成Map
     *
     * @param request
     * @return
     */
    private Map<String, String> convertRequestParamsToMap(HttpServletRequest request) {
        Map<String, String> params = new HashMap<String, String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(ObjectUtils.underlineToCamel(name), valueStr);
        }
        LOG.info("request params = {}", params);
        return params;
    }

    private Map<String, String> convertRequestParamsToMapRaw(HttpServletRequest request) {
        Map<String, String> params = new HashMap<String, String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        LOG.info("request raw params = {}", params);
        return params;
    }

    private AliRecallParam buildAlipayNotifyParam(Map<String, String> params) {
        String json = JSON.toJSONString(params);
        return JSON.parseObject(json, AliRecallParam.class);
    }
}
