package io.pay.modules.app.controller;

import io.pay.common.utils.JsonHelper;
import io.pay.modules.app.form.AliRecallParam;
import io.pay.modules.app.service.OrderInfoDBService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by laaaa on 2018/12/16.
 */
@RestController
@RequestMapping("/")
public class TestController {
    private static final Logger LOG = LoggerFactory.getLogger(TestController.class);
    @Autowired
    OrderInfoDBService orderInfoDBService;

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> processOrder(HttpServletRequest request) {
        LOG.info("for testdddddddddddddddddd");
        AliRecallParam aliRecallParam = new AliRecallParam();
        aliRecallParam.setBody("dd");
        aliRecallParam.setBuyerEmail("dd");
        aliRecallParam.setBuyerId("s1");
        aliRecallParam.setBuyerLogonId("long");
        aliRecallParam.setDiscount("discount");
        aliRecallParam.setInvoiceAmount(0.88);
        aliRecallParam.setReceiptAmount(0.88);
        aliRecallParam.setTotalAmount(0.8);
        aliRecallParam.setPointAmount(0.98);
        aliRecallParam.setBuyerPayAmount("0.57");
        orderInfoDBService.insertOrderInfo(aliRecallParam);
        return JsonHelper.jsonpEntity("success", null);
    }


    @RequestMapping("/home")
    public void home(HttpServletRequest request, HttpServletResponse response) {
        try {
            response.sendRedirect("static/fillOrder.html");
        } catch (Exception e){
            return ;
        }
    }
}