package io.pay.modules.app.service.impl;

import com.alipay.api.request.AlipayTradeOrderSettleRequest;
import com.alipay.api.response.AlipayTradeOrderSettleResponse;
import com.google.common.collect.Lists;
import io.pay.common.consts.AlipayConsts;
import io.pay.common.utils.DateUtils;
import io.pay.common.utils.JsonUtils;
import io.pay.modules.app.ali.PcAlipayClient;
import io.pay.modules.app.model.AlipayTradeOrderSettleDO;
import io.pay.modules.app.service.AlipayTradeOrderSettleService;
import io.pay.modules.app.service.OrderInfoDBService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-02-14
 * @description:
 */
@Service
public class AlipayTradeOrderSettleServiceImpl implements AlipayTradeOrderSettleService {
    @Autowired
    private PcAlipayClient pcAlipayClient;

    @Autowired
    OrderInfoDBService orderInfoDBService;

    /*
    必填字段
    * out_request_no String 自己生成, 结算流水, 保存记录
    * trade_no String: 支付宝订单号
    * royalty_parameters OpenApiRoyaltyDetailInfoPojo[]	 必填，但是数组内容均为可填
    *  trans_out 支付方支付宝ID
    *  trans_in
    *  amount
    *  amount_percentage
    *  desc
    *
    * */

    @Override
    public AlipayTradeOrderSettleResponse orderSettle(String outRequestNo,
                                                      String outTradeNo,
                                                      String tradeNo,
                                                      String transOut,
                                                      String transIn,
                                                      Double amount,
                                                      Integer amountPercentage,
                                                      String desc,
                                                      String appAuthToken) {
        String bizContent = buildBizContent(outRequestNo, tradeNo, transOut, transIn, amount, amountPercentage, desc);

        AlipayTradeOrderSettleResponse response = orderSettleByBizContent(bizContent, appAuthToken);
        //插入数据库
        AlipayTradeOrderSettleDO tradeOrderSettleDO = new AlipayTradeOrderSettleDO();
        tradeOrderSettleDO.setOutRequestNo(outRequestNo);
        tradeOrderSettleDO.setOutTradeNo(outTradeNo);
        tradeOrderSettleDO.setTradeNo(tradeNo);
        tradeOrderSettleDO.setTransOut(transOut);
        tradeOrderSettleDO.setTransIn(transIn);
        tradeOrderSettleDO.setAmount(amount);
        tradeOrderSettleDO.setAmountPercentage(amountPercentage);
        tradeOrderSettleDO.setDesc(desc);
        tradeOrderSettleDO.setResponseTradeNo(response.getTradeNo());
        tradeOrderSettleDO.setCode(response.getCode());
        tradeOrderSettleDO.setMsg(response.getMsg());
        tradeOrderSettleDO.setSubCode(response.getSubCode());
        tradeOrderSettleDO.setSubMsg(response.getSubMsg());
        tradeOrderSettleDO.setBody(response.getBody());
        tradeOrderSettleDO.setParams(JsonUtils.toJsonString(response.getParams()));
        tradeOrderSettleDO.setCtime(System.currentTimeMillis());
        tradeOrderSettleDO.setUtime(System.currentTimeMillis());
        tradeOrderSettleDO.setGmtCreate(DateUtils.getCurrentTime());
        orderInfoDBService.insertAlipayTradeOrderSettleInfo(tradeOrderSettleDO);

        return response;
    }

    private AlipayTradeOrderSettleResponse orderSettleByBizContent(String content, String appAuthToken) {
        AlipayTradeOrderSettleRequest request = new AlipayTradeOrderSettleRequest();
        request.setBizContent(content);
        return pcAlipayClient.execute(request, appAuthToken, false);
    }

    private String buildBizContent(String outRequestNo,
                                   String tradeNo,
                                   String transOut,
                                   String transIn,
                                   Double amount,
                                   Integer amountPercentage,
                                   String desc) {
        Map<String, Object> content = new HashMap<String, Object>();
        List<Map<String, Object>> royaltyParameters = Lists.newArrayList();
        Map<String, Object> royaltyParameter = new HashMap<String, Object>();
        royaltyParameter.put(AlipayConsts.FIELD_NAME_TRANS_OUT, transOut);
        royaltyParameter.put(AlipayConsts.FIELD_NAME_TRANS_IN, transIn);
        royaltyParameter.put(AlipayConsts.FIELD_NAME_AMOUNT, amount);
        //royaltyParameter.put(AlipayConsts.FIELD_NAME_AMOUNT_PERCENTAGE, amountPercentage);
        royaltyParameter.put(AlipayConsts.FIELD_NAME_DESC, desc);
        royaltyParameters.add(royaltyParameter);

        content.put(AlipayConsts.FIELD_NAME_OUT_REQUEST_NO, outRequestNo);
        content.put(AlipayConsts.FIELD_NAME_TRADE_NO, tradeNo);
        content.put(AlipayConsts.FIELD_NAME_ROYALTY_PARAMETERS, royaltyParameters);
        return JsonUtils.toJsonString(content);
    }
}
