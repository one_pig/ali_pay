package io.pay.modules.app.service.impl;

import io.pay.common.utils.DateUtils;
import io.pay.modules.app.dao.IAlipaySellerInfoDao;
import io.pay.modules.app.dao.IOrderInfoDao;
import io.pay.modules.app.model.AlipaySellerTradeInfoDO;
import io.pay.modules.app.service.PcOrderManageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-03-03
 * @description:
 */
@Service
public class PcOrderManageServiceImpl implements PcOrderManageService {
    private static final Logger LOG = LoggerFactory.getLogger(PcOrderManageServiceImpl.class);

    public String mazhuAppAuthToken = "201804BB0c2604ac348d4470854aebfe8540dD33";

    @Resource
    IAlipaySellerInfoDao alipaySellerInfoDaoMysqlImpl;

    @Resource
    IOrderInfoDao orderInfoDaoMysqlImpl;

    public String generateOutTradeNo(){
        return "tradepay" + System.currentTimeMillis()
                + (long) (Math.random() * 10000000L);
    }

    /**
     * 码主选择逻辑
     * 优先选择: 没有交易过的, 且符合单笔最大的, 然后按available_gmv最大的
     *
     * @param money
     * @return
     */
    @Override
    public String selectSeller(Double money) {
        LOG.info("selectSeller start");
        List<AlipaySellerTradeInfoDO> candidateSellers = alipaySellerInfoDaoMysqlImpl.getSellerTradeInfos(DateUtils.getCurrentTime(DateUtils.DATE_PATTERN) + " 00:00:00");
        LOG.info("candidateSellers size={}",candidateSellers.size());
        LOG.info("candidateSellers sample={}",(candidateSellers.size() > 0 ? candidateSellers.get(0).toString() : "empty"));
        List<AlipaySellerTradeInfoDO> availableSellers = candidateSellers.stream().filter(sellInfo -> sellInfo.getAvailableGmv() >= money && sellInfo.getSellerQuota() >= money.intValue()).collect(Collectors.toList());
        int availableCounts = availableSellers.size();
        LOG.info("Limit gmv&quota. availableSellers size={}",availableCounts);
        if (availableCounts > 0) {
            int index = (int)System.currentTimeMillis() % availableCounts;
            return availableSellers.get(index).getUserId();
        } else {
            List<AlipaySellerTradeInfoDO> degradeAvailableSellers = candidateSellers.stream().filter(sellInfo -> sellInfo.getSellerQuota() > money.intValue()).collect(Collectors.toList());
            int degradeAvailableCounts = degradeAvailableSellers.size();
            LOG.info("Limit gmv&quota. degradeAvailableSellers size={}",degradeAvailableCounts);
            if (degradeAvailableCounts > 0) {
                int degradeIndex = (int)System.currentTimeMillis() % degradeAvailableCounts;
                return degradeAvailableSellers.get(degradeIndex).getUserId();
            } else {
                return null;
            }
        }
    }

    @Override
    public String getAppAuthToken() {
        LOG.info("appAuthToken="+mazhuAppAuthToken);
        return mazhuAppAuthToken;
    }
}
