package io.pay.modules.app.service.impl;

import io.pay.common.utils.QRCodeUtil;
import io.pay.modules.app.dao.*;
import io.pay.modules.app.model.*;
import io.pay.modules.app.service.ManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
public class ManagerServiceImpl implements ManagerService {
    private static final Logger LOG = LoggerFactory.getLogger(ManagerServiceImpl.class);

    @Resource
    IPreOrderInfoDao preOrderInfoDaoMysqlImpl;

    @Resource
    IAlipaySellerInfoDao alipaySellerInfoDaoMysqlImpl;

    @Resource
    IOrderInfoDao orderInfoDaoMysqlImpl;

    @Resource
    IAlipayFundTransDao alipayFundTransDaoMysqlImpl;

    @Resource
    IAlipayTradeOrderSettleDao alipayTradeOrderSettleDaoMysqlImpl;

    @Override
    public List<PreOrderInfoDO> getPreOrderByDate() {
        List<PreOrderInfoDO> preOrderInfoList = preOrderInfoDaoMysqlImpl.selectAll();
        return preOrderInfoList;
    }

    @Override
    public List<OrderInfoDO> getOrderInfoInfos() {
        return orderInfoDaoMysqlImpl.selectAll();
    }

    @Override
    public List<AlipayTradeOrderSettleDO> getOrderSettleDOs() {
        return alipayTradeOrderSettleDaoMysqlImpl.selectAll();
    }

    @Override
    public List<AlipayFundTransDO> getFundTransDOs() {
        return alipayFundTransDaoMysqlImpl.selectAll();
    }

    /**
     * 查询所有码主信息
     *
     * @return
     */
    @Override
    public List<AlipaySellerInfoDO> getAlipaySellerInfos() {
        return alipaySellerInfoDaoMysqlImpl.selectAll();
    }

    /**
     * private String userId;
     * private String sellerDescription;
     * private String sellerRate;
     * private String sellerDailyGmv;
     * private String sellerQuota;
     * private String state;
     * private String gmtCreate;
     *
     * @return
     */
    @Override
    public List<AlipaySellerInfoVO> getAlipaySellerInfoVOs(HttpServletRequest request) {
        List<AlipaySellerInfoVO> alipaySellerInfoDOs = alipaySellerInfoDaoMysqlImpl.selectAllUsers();
        List<AlipaySellerInfoVO> alipaySellerInfoVOs = new ArrayList<>();
        for (AlipaySellerInfoVO alipaySellerInfoDO : alipaySellerInfoDOs) {
            AlipaySellerInfoVO alipaySellerInfoVO = new AlipaySellerInfoVO();
            alipaySellerInfoVO.setId(alipaySellerInfoDO.getId());
            alipaySellerInfoVO.setUserId(alipaySellerInfoDO.getUserId());
            String sellerDescription = "<input type='text' class='form-control' id='sellerDescription" + alipaySellerInfoDO.getId() + "' " + " value='" + alipaySellerInfoDO.getSellerDescription() + "'/>";
            alipaySellerInfoVO.setSellerDescription(sellerDescription);
            String sellerRate = "<input type='text' class='form-control' id='sellerRate" + alipaySellerInfoDO.getId() + "' " + " value='" + alipaySellerInfoDO.getSellerRate() + "'/>";
            alipaySellerInfoVO.setSellerRate(sellerRate);
            String sellerDailyGmv = "<input type='text' class='form-control' id='sellerDailyGmv" + alipaySellerInfoDO.getId() + "' " + " value='" + alipaySellerInfoDO.getSellerDailyGmv() + "'/>";
            alipaySellerInfoVO.setSellerDailyGmv(sellerDailyGmv);
            String sellerQuota = "<input type='text' class='form-control' id='sellerQuota" + alipaySellerInfoDO.getId() + "' " + " value='" + alipaySellerInfoDO.getSellerQuota() + "'/>";
            alipaySellerInfoVO.setSellerQuota(sellerQuota);
            String state = "<input type='text' class='form-control' id='state" + alipaySellerInfoDO.getId() + "' " + " value='" + alipaySellerInfoDO.getState() + "'/>";
            alipaySellerInfoVO.setState(state);
            String updateContent = "<button id='update' class='btn btn-block btn-primary' onclick='updateFunction(" + alipaySellerInfoDO.getId() + ")'>提交修改</button>";
            alipaySellerInfoVO.setOperation(updateContent);
            alipaySellerInfoVO.setGmtCreate(alipaySellerInfoDO.getGmtCreate());
            alipaySellerInfoVO.setNickName(alipaySellerInfoDO.getNickName());
            alipaySellerInfoVO.setCityName(alipaySellerInfoDO.getCityName());
            String fileName = request.getContextPath() + "/static/src/img/" + alipaySellerInfoDO.getUserId() + ".jpg";
            File file = new File(fileName);
            String userId = alipaySellerInfoDO.getUserId();
            if (!file.exists()){
                String destPath = request.getSession().getServletContext().getRealPath("/") + "/static/src/img/";

                try {
                    QRCodeUtil.encode("http://47.105.68.91:8080/alipay/validate?sellerId=" + userId, null, destPath, false, userId);
                } catch (Exception e) {
                    LOG.error(e.toString());
                }
            }
            alipaySellerInfoVO.setQrCodePath("<button id='" + userId + "' class='btn btn-block btn-primary' onclick='displayPic(\"" + fileName +  "\")'>验证支付码</button>");

            alipaySellerInfoVOs.add(alipaySellerInfoVO);

        }

        return alipaySellerInfoVOs;
    }

    /**
     * 更新指定码主信息
     *
     * @param alipaySellerInfoDO
     */
    @Override
    public void update(AlipaySellerInfoDO alipaySellerInfoDO) {
        alipaySellerInfoDaoMysqlImpl.update(alipaySellerInfoDO);
    }

    @Override
    public PreOrderInfoDO selectByOutTradeNo(String outTradeNo) {
        PreOrderInfoDO preOrderInfo = preOrderInfoDaoMysqlImpl.selectByOutTradeNo(outTradeNo);
        return preOrderInfo;
    }

}
