package io.pay.modules.app.service;

import io.pay.modules.app.model.AlipaySellerInfoDO;

/**
 * Created by laaaa on 2018/12/16.
 */
public interface DatabaseService {
    public AlipaySellerInfoDO selectSellerInfoBySellerId(String sellerId);

    /**
     * 码主选择逻辑
     * 优先选择: 没有交易过的, 且符合单笔最大的, 然后按available_gmv最大的
     *
     * @param money
     * @return
     */
    public String selectSellerIdByMoney(Double money) ;
}
