package io.pay.modules.app.service.impl;

import com.alipay.api.request.AlipayOpenAuthTokenAppQueryRequest;
import com.alipay.api.request.AlipayOpenAuthTokenAppRequest;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.request.AlipayUserInfoShareRequest;
import com.alipay.api.response.AlipayOpenAuthTokenAppQueryResponse;
import com.alipay.api.response.AlipayOpenAuthTokenAppResponse;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.alipay.api.response.AlipayUserInfoShareResponse;
import io.pay.common.consts.AlipayConsts;
import io.pay.common.utils.DateUtils;
import io.pay.common.utils.JsonUtils;
import io.pay.modules.app.ali.PcAlipayClient;
import io.pay.modules.app.dao.IAlipaySellerInfoDao;
import io.pay.modules.app.dao.IAlipaySellerUserInfoDao;
import io.pay.modules.app.model.AlipaySellerInfoDO;
import io.pay.modules.app.model.AlipaySellerUserInfoDO;
import io.pay.modules.app.service.AlipayAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-04-01
 * @description:
 */
@Service
public class AlipayAuthServiceImpl implements AlipayAuthService {
    private static final Logger LOG = LoggerFactory.getLogger(AlipayAuthServiceImpl.class);

    @Autowired
    private PcAlipayClient pcAlipayClient;

    @Resource
    IAlipaySellerInfoDao alipaySellerInfoDaoMysqlImpl;

    @Resource
    IAlipaySellerUserInfoDao alipaySellerUserInfoDaoMysqlImpl;

    @Override
    public AlipayOpenAuthTokenAppResponse alipayOpenAuthTokenApp(String grantType,
                                                                 String code) {
        String bizContent = buildBizContent(grantType, code);
        return alipayOpenAuthTokenAppByBizContent(bizContent);
    }

    private AlipayOpenAuthTokenAppResponse alipayOpenAuthTokenAppByBizContent(String content) {
        AlipayOpenAuthTokenAppRequest request = new AlipayOpenAuthTokenAppRequest();
        request.setBizContent(content);
        AlipayOpenAuthTokenAppResponse response = pcAlipayClient.execute(request);
        //码主入驻信息入库
        AlipaySellerInfoDO alipaySellerInfoDO = new AlipaySellerInfoDO();
        alipaySellerInfoDO.setAuthAppId(response.getAuthAppId());
        alipaySellerInfoDO.setAppAuthToken(response.getAppAuthToken());
        alipaySellerInfoDO.setAppRefreshToken(response.getAppRefreshToken());
        alipaySellerInfoDO.setUserId(response.getUserId());
        alipaySellerInfoDO.setExpiresIn(response.getExpiresIn());
        alipaySellerInfoDO.setReExpiresIn(response.getReExpiresIn());
        alipaySellerInfoDO.setCtime(System.currentTimeMillis());
        alipaySellerInfoDO.setUtime(System.currentTimeMillis());
        alipaySellerInfoDO.setGmtCreate(DateUtils.getCurrentTime());
        alipaySellerInfoDO.setState(response.isSuccess() ? 1 : 0);
        if (!response.isSuccess()) {
            alipaySellerInfoDO.setSellerDescription("msg:"+response.getMsg() + ". submsg:"+response.getSubMsg());
        }

        if (response.isSuccess()) {
            try {
                alipaySellerInfoDaoMysqlImpl.delete(alipaySellerInfoDO.getUserId());
            } catch (Exception e) {
                LOG.error("删除用户异常,userId={}",alipaySellerInfoDO.getUserId());
            }
        }
        alipaySellerInfoDaoMysqlImpl.insert(alipaySellerInfoDO);

        return response;
    }

    private String buildBizContent(String grantType,
                                   String code) {
        Map<String, Object> content = new HashMap<String, Object>();
        content.put(AlipayConsts.FIELD_NAME_GRANT_TYPE, grantType);
        content.put(AlipayConsts.FIELD_NAME_CODE, code);
        return JsonUtils.toJsonString(content);
    }

    @Override
    public AlipayOpenAuthTokenAppQueryResponse alipayOpenAuthTokenQuery(String appAuthToken){
        AlipayOpenAuthTokenAppQueryRequest request = new AlipayOpenAuthTokenAppQueryRequest();
        String bizContent = buildBizContentForAuthQuery(appAuthToken);
        return alipayOpenAuthTokenQueryBizContent(bizContent);
    }

    private AlipayOpenAuthTokenAppQueryResponse alipayOpenAuthTokenQueryBizContent(String bizContent){
        AlipayOpenAuthTokenAppQueryRequest request = new AlipayOpenAuthTokenAppQueryRequest();
        request.setBizContent(bizContent);
        return pcAlipayClient.execute(request);
    }

    private String buildBizContentForAuthQuery(String appAuthToken){
        Map<String, Object> content = new HashMap<String, Object>();
        content.put(AlipayConsts.FIELD_NAME_APP_AUTH_TOKEN, appAuthToken);
        return JsonUtils.toJsonString(content);
    }


    @Override
    public AlipaySystemOauthTokenResponse alipaySystemOauthToken(String authCode){
        AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
        request.setCode(authCode);
        request.setGrantType("authorization_code");
        return pcAlipayClient.execute(request);
    }

    @Override
    public AlipayUserInfoShareResponse alipayGetUserInfo(String authToken) {
        AlipayUserInfoShareRequest request = new AlipayUserInfoShareRequest();
        AlipayUserInfoShareResponse response = pcAlipayClient.execute(request, authToken, true);
        AlipaySellerUserInfoDO alipaySellerUserInfoDO = new AlipaySellerUserInfoDO();
        alipaySellerUserInfoDO.setUserId(response.getUserId());
        alipaySellerUserInfoDO.setNickName(response.getNickName());
        alipaySellerUserInfoDO.setAvatar(response.getAvatar());
        alipaySellerUserInfoDO.setCity(response.getCity());
        alipaySellerUserInfoDO.setProvince(response.getProvince());

        alipaySellerUserInfoDO.setCtime(System.currentTimeMillis());
        alipaySellerUserInfoDO.setUtime(System.currentTimeMillis());
        alipaySellerUserInfoDO.setGmtCreate(DateUtils.getCurrentTime());
        alipaySellerUserInfoDO.setState(response.isSuccess() ? 1 : 0);

        if (response.isSuccess()) {
            try {
                alipaySellerUserInfoDaoMysqlImpl.delete(alipaySellerUserInfoDO.getUserId());
            } catch (Exception e) {
                LOG.error("删除用户异常,userId={}",alipaySellerUserInfoDO.getUserId());
            }
        }
        alipaySellerUserInfoDaoMysqlImpl.insert(alipaySellerUserInfoDO);

        return response;
    }
}
