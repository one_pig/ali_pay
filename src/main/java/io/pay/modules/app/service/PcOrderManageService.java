package io.pay.modules.app.service;

/**
 * Created by laaaa on 2018/12/16.
 */
public interface PcOrderManageService {
    /**
     * 码主选择逻辑
     * 优先选择: 没有交易过的, 且符合单笔最大的, 然后按available_gmv最大的
     *
     * @param money
     * @return
     */
    public String selectSeller(Double money);

    public String getAppAuthToken();
}
