package io.pay.modules.app.service.impl;

import com.google.gson.Gson;
import io.pay.modules.app.dao.IAlipayFundTransDao;
import io.pay.modules.app.dao.IAlipayTradeOrderSettleDao;
import io.pay.modules.app.dao.IOrderInfoDao;
import io.pay.modules.app.form.AliRecallParam;
import io.pay.modules.app.model.AlipayFundTransDO;
import io.pay.modules.app.model.AlipayTradeOrderSettleDO;
import io.pay.modules.app.model.OrderInfoDO;
import io.pay.modules.app.service.OrderInfoDBService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OrderInfoDBServiceImpl implements OrderInfoDBService {
    private static final Logger LOG = LoggerFactory.getLogger(OrderInfoDBServiceImpl.class);

    @Resource
    IOrderInfoDao orderInfoDaoMysqlImpl;

    @Resource
    IAlipayTradeOrderSettleDao alipayTradeOrderSettleDaoMysqlImpl;

    @Resource
    IAlipayFundTransDao alipayFundTransDaoMysqlImpl;

    @Override
    public void insertOrderInfo(AliRecallParam aliRecallParam){
        Gson gson = new Gson();
        OrderInfoDO orderInfoDO = new OrderInfoDO();
        orderInfoDO.setOutTradeNo(aliRecallParam.getOutTradeNo());
        orderInfoDO.setTradeNo(aliRecallParam.getTradeNo());
        orderInfoDO.setOrderSubject(aliRecallParam.getSubject());
        orderInfoDO.setSellerId(aliRecallParam.getSellerId());
        orderInfoDO.setSellerEmail(aliRecallParam.getSellerEmail());
        orderInfoDO.setBuyerId(aliRecallParam.getBuyerId());
        orderInfoDO.setBuyerLogonId(aliRecallParam.getBuyerLogonId());
        orderInfoDO.setBuyerPayAmount(Double.parseDouble(aliRecallParam.getBuyerPayAmount()));
        orderInfoDO.setNotifyId(aliRecallParam.getNotifyId());
        orderInfoDO.setNotifyType(aliRecallParam.getNotifyType());
        orderInfoDO.setTradeStatus(aliRecallParam.getTradeStatus());
        orderInfoDO.setFundBillList(gson.toJson(aliRecallParam.getFundBillList()));
        orderInfoDO.setInvoiceAmount(aliRecallParam.getInvoiceAmount());
        orderInfoDO.setReceiptAmount(aliRecallParam.getReceiptAmount());
        orderInfoDO.setTotalAmount(aliRecallParam.getTotalAmount());
        orderInfoDO.setPointAmount(aliRecallParam.getPointAmount());
        orderInfoDO.setGmtCreate(aliRecallParam.getGmtCreate());
        orderInfoDO.setGmtPayment(aliRecallParam.getGmtPayment());
        orderInfoDO.setNotifyTime(aliRecallParam.getNotifyTime());
        orderInfoDO.setCtime(System.currentTimeMillis());
        orderInfoDO.setUtime(System.currentTimeMillis());
        orderInfoDaoMysqlImpl.insert(orderInfoDO);
    }

    @Override
    public void insertAlipayTradeOrderSettleInfo(AlipayTradeOrderSettleDO alipayTradeOrderSettleDO){
        alipayTradeOrderSettleDaoMysqlImpl.insert(alipayTradeOrderSettleDO);
    }

    @Override
    public void insertAlipayFundTransInfo(AlipayFundTransDO alipayFundTransDO){
        alipayFundTransDaoMysqlImpl.insert(alipayFundTransDO);
    }

    @Override
    public List<String> selectSellersByBuyerId(String buyerId, String date){
        return orderInfoDaoMysqlImpl.selectByUserId(buyerId, date);
    }


}
