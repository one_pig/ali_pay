package io.pay.modules.app.service;

import com.alipay.api.response.AlipayOpenAuthTokenAppQueryResponse;
import com.alipay.api.response.AlipayOpenAuthTokenAppResponse;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.alipay.api.response.AlipayUserInfoShareResponse;

/**
 * Created by laaaa on 2018/12/16.
 */
public interface AlipayAuthService {
    public AlipayOpenAuthTokenAppResponse alipayOpenAuthTokenApp(String grantType,
                                                                 String code);

    public AlipayOpenAuthTokenAppQueryResponse alipayOpenAuthTokenQuery(String appAuthToken);

    public AlipaySystemOauthTokenResponse alipaySystemOauthToken(String authCode);

    public AlipayUserInfoShareResponse alipayGetUserInfo(String authToken);
}
