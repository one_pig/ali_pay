package io.pay.modules.app.service.impl;

import io.pay.common.utils.DateUtils;
import io.pay.modules.app.dao.IAlipaySellerInfoDao;
import io.pay.modules.app.model.AlipaySellerInfoDO;
import io.pay.modules.app.model.AlipaySellerTradeInfoDO;
import io.pay.modules.app.service.DatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DatabaseServiceImpl implements DatabaseService {
    private static final Logger LOG = LoggerFactory.getLogger(DatabaseServiceImpl.class);

    @Resource
    IAlipaySellerInfoDao alipaySellerInfoDaoMysqlImpl;

    public AlipaySellerInfoDO selectSellerInfoBySellerId(String sellerId) {
        return alipaySellerInfoDaoMysqlImpl.selectByUserId(sellerId);
    }


    /**
     * 码主选择逻辑
     * 优先选择: 没有交易过的, 且符合单笔最大的, 然后按available_gmv最大的
     *
     * @param money
     * @return
     */
    @Override
    public String selectSellerIdByMoney(Double money) {
        List<AlipaySellerTradeInfoDO> candidateSellers = alipaySellerInfoDaoMysqlImpl.getSellerTradeInfos(DateUtils.getCurrentTime(DateUtils.DATE_PATTERN) + " 00:00:00");
        LOG.info("candidateSellers size={}",candidateSellers.size());
        LOG.info("candidateSellers sample={}",(candidateSellers.size() > 0 ? candidateSellers.get(0).toString() : "empty"));
        List<AlipaySellerTradeInfoDO> availableSellers = candidateSellers.stream().filter(sellInfo -> sellInfo.getAvailableGmv() >= money && sellInfo.getSellerQuota() >= money.intValue()).collect(Collectors.toList());
        int availableCounts = availableSellers.size();
        LOG.info("Limit gmv&quota. availableSellers size={}",availableCounts);
        if (availableCounts > 0) {
            int index = (int)System.currentTimeMillis() % availableCounts;
            return availableSellers.get(index).getUserId();
        } else {
            List<AlipaySellerTradeInfoDO> degradeAvailableSellers = candidateSellers.stream().filter(sellInfo -> sellInfo.getSellerQuota() > money.intValue()).collect(Collectors.toList());
            int degradeAvailableCounts = degradeAvailableSellers.size();
            LOG.info("Limit gmv&quota. degradeAvailableSellers size={}",degradeAvailableCounts);
            if (degradeAvailableCounts > 0) {
                int degradeIndex = (int)System.currentTimeMillis() % degradeAvailableCounts;
                return degradeAvailableSellers.get(degradeIndex).getUserId();
            } else {
                return null;
            }
        }
    }

}
