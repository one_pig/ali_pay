package io.pay.modules.app.service;

import io.pay.modules.app.form.AliRecallParam;
import io.pay.modules.app.model.AlipayFundTransDO;
import io.pay.modules.app.model.AlipayTradeOrderSettleDO;

import java.util.List;

/**
 * Created by laaaa on 2018/12/16.
 */
public interface OrderInfoDBService {

    public void insertOrderInfo(AliRecallParam aliRecallParam);

    public void insertAlipayTradeOrderSettleInfo(AlipayTradeOrderSettleDO alipayTradeOrderSettleDO);

    public void insertAlipayFundTransInfo(AlipayFundTransDO alipayFundTransDO);

    public List<String> selectSellersByBuyerId(String buyerId, String date);
}
