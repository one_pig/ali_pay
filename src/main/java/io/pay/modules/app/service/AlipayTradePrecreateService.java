package io.pay.modules.app.service;

import com.alipay.api.response.AlipayTradePrecreateResponse;

/**
 * Created by laaaa on 2018/12/16.
 */
public interface AlipayTradePrecreateService {
    public AlipayTradePrecreateResponse precreateTrade(String outTradeNo,
                                                       String sellerId,
                                                       Double totalAmount,
                                                       String subject,
                                                       String notifyUrl,
                                                       String appAuthToken);
}
