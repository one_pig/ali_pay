package io.pay.modules.app.service.impl;

import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import io.pay.common.consts.AlipayConsts;
import io.pay.common.utils.JsonUtils;
import io.pay.modules.app.ali.PcAlipayClient;
import io.pay.modules.app.service.AlipayTradePrecreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-03-03
 * @description:
 */
@Service
public class AlipayTradePrecreateServiceImpl implements AlipayTradePrecreateService {
    @Autowired
    private PcAlipayClient pcAlipayClient;

    /*
    必填字段
    * out_trade_no String
    * seller_id String: 码主支付宝ID
    * total_amount Price
    * subject String: 订单标题
    * notifyUrl String: 回调地址
    * */

    @Override
    public AlipayTradePrecreateResponse precreateTrade(String outTradeNo,
                                                       String sellerId,
                                                       Double totalAmount,
                                                       String subject,
                                                       String notifyUrl,
                                                       String appAuthToken) {
        String content = buildBizContent(outTradeNo, sellerId, totalAmount, subject, notifyUrl);
        return precreateTradeByBizContent(content, notifyUrl, appAuthToken);
    }

    private AlipayTradePrecreateResponse precreateTradeByBizContent(String content, String notifyUrl, String appAuthToken) {
        AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();
        request.setNotifyUrl(notifyUrl);
        request.setBizContent(content);
        return pcAlipayClient.execute(request, appAuthToken, false);
    }

    private String buildBizContent(String outTradeNo, String sellerId, Double totalAmount, String subject, String notifyUrl) {
        Map<String, Object> content = new HashMap<String, Object>();
        content.put(AlipayConsts.FIELD_NAME_OUT_TRADE_NO, outTradeNo);
        content.put(AlipayConsts.FIELD_NAME_SELLER_ID, sellerId);
        content.put(AlipayConsts.FIELD_NAME_TOTAL_AMOUNT, totalAmount);
        content.put(AlipayConsts.FIELD_NAME_SUBJECT, subject);
        return JsonUtils.toJsonString(content);
    }
}
