package io.pay.modules.app.service;

import com.alipay.api.response.AlipayFundTransToaccountTransferResponse;

/**
 * Created by laaaa on 2018/12/16.
 */
public interface AlipayFundService {
    public AlipayFundTransToaccountTransferResponse alipayFundTransToaccount(String outTradeNo,
                                                                             String outBizNo,
                                                                             String payeeType,
                                                                             String payeeAccount,
                                                                             Double amount,
                                                                             String payerShowName,
                                                                             String remark);
}
