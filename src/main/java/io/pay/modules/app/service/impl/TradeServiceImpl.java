package io.pay.modules.app.service.impl;


import com.alipay.api.response.AlipayFundTransToaccountTransferResponse;
import com.alipay.api.response.AlipayTradeOrderSettleResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import io.pay.common.utils.DateUtils;
import io.pay.modules.app.dao.IPreOrderInfoDao;
import io.pay.modules.app.model.PreOrderInfoDO;
import io.pay.modules.app.model.SettleResult;
import io.pay.modules.app.model.TransferResult;
import io.pay.modules.app.service.AlipayTradePrecreateService;
import io.pay.modules.app.service.TradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TradeServiceImpl implements TradeService {
    private static final Logger log = LoggerFactory.getLogger(TradeServiceImpl.class);
    @Autowired
    private AlipayTradePrecreateService alipayTradePrecreateService;

    @Autowired
    private AlipayTradeOrderSettleServiceImpl alipayTradeOrderSettleService;

    @Autowired
    private AlipayFundServiceImpl alipayFundTransToaccountTransfer;

    @Resource
    IPreOrderInfoDao preOrderInfoDaoMysqlImpl;

    @Override
    public AlipayTradePrecreateResponse precreateTrade(String outTradeNo, String sellerId, Double totalAmount, String subject, String notifyUrl, String appAuthToken) {
        AlipayTradePrecreateResponse response = alipayTradePrecreateService.precreateTrade(outTradeNo, sellerId, totalAmount, subject, notifyUrl, appAuthToken);
        PreOrderInfoDO preOrderInfoDO = new PreOrderInfoDO();
        preOrderInfoDO.setOutTradeNo(outTradeNo);
        preOrderInfoDO.setSellerId(sellerId);
        preOrderInfoDO.setTotalAmount(totalAmount);
        preOrderInfoDO.setOrderSubject(subject);
        preOrderInfoDO.setNotifyUrl(notifyUrl);
        preOrderInfoDO.setQrCode(response.getQrCode());
        preOrderInfoDO.setCtime(System.currentTimeMillis());
        preOrderInfoDO.setUtime(System.currentTimeMillis());
        preOrderInfoDO.setGmtCreate(DateUtils.getCurrentTime());
        preOrderInfoDaoMysqlImpl.insert(preOrderInfoDO);
        log.info("preOrderInfo insert {}", preOrderInfoDO.toString());

        return response;
    }

    @Override
    public SettleResult settle(String outRequestNo, String tradeNo, Double money) {
        AlipayTradeOrderSettleResponse r = alipayTradeOrderSettleService.orderSettle("", outRequestNo, tradeNo, "码组商家淘宝id", "委托人商家淘宝id", money, 100, "测试的转账","");
        return new SettleResult(tradeNo);
    }

    @Override
    public TransferResult transfer(String outBizNo, String payeeType, String payeeAccount, String amount) {
        AlipayFundTransToaccountTransferResponse r = alipayFundTransToaccountTransfer.alipayFundTransToaccount(
                "",
                "自己生成的转账唯一标识ID",
                payeeType,
                payeeAccount,
                Double.valueOf(amount),
                "付款方姓名（如上海交通卡退款）",
                "转账备注"
        );
        return new TransferResult(r.getOutBizNo(), r.getOrderId(), r.getPayDate());
    }


}