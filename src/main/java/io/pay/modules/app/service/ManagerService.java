package io.pay.modules.app.service;

import io.pay.modules.app.model.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by laaaa on 2018/12/16.
 */
public interface ManagerService {
    public List<PreOrderInfoDO> getPreOrderByDate();

    public List<OrderInfoDO> getOrderInfoInfos();

    public List<AlipayTradeOrderSettleDO> getOrderSettleDOs();

    public List<AlipayFundTransDO> getFundTransDOs();

    /**
     * 查询所有码主信息
     *
     * @return
     */
    public List<AlipaySellerInfoDO> getAlipaySellerInfos();
    /**
     * private String userId;
     * private String sellerDescription;
     * private String sellerRate;
     * private String sellerDailyGmv;
     * private String sellerQuota;
     * private String state;
     * private String gmtCreate;
     *
     * @return
     */
    public List<AlipaySellerInfoVO> getAlipaySellerInfoVOs(HttpServletRequest request) ;

    /**
     * 更新指定码主信息
     *
     * @param alipaySellerInfoDO
     */
    public void update(AlipaySellerInfoDO alipaySellerInfoDO) ;


    public PreOrderInfoDO selectByOutTradeNo(String outTradeNo) ;

}
