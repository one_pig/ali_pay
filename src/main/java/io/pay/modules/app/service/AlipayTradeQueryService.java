package io.pay.modules.app.service;

import com.alipay.api.response.AlipayTradeQueryResponse;

/**
 * Created by laaaa on 2018/12/16.
 */
public interface AlipayTradeQueryService {
    public AlipayTradeQueryResponse alipayTradeQuery(String tradeNo);
}
