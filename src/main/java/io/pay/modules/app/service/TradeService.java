package io.pay.modules.app.service;


import com.alipay.api.response.AlipayTradePrecreateResponse;
import io.pay.modules.app.model.SettleResult;
import io.pay.modules.app.model.TransferResult;

public interface TradeService {
    public AlipayTradePrecreateResponse precreateTrade(String outTradeNo, String sellerId, Double totalAmount, String subject, String notifyUrl, String appAuthToken);
    public SettleResult settle(String outRequestNo, String tradeNo, Double money);
    public TransferResult transfer(String outBizNo, String payeeType, String payeeAccount, String amount);
}
