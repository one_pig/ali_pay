package io.pay.modules.app.service.impl;

import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeQueryResponse;
import io.pay.common.consts.AlipayConsts;
import io.pay.common.utils.JsonUtils;
import io.pay.modules.app.ali.PcAlipayClient;
import io.pay.modules.app.service.AlipayTradeQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-02-22
 * @description:
 */
@Service
public class AlipayTradeQueryServiceImpl implements AlipayTradeQueryService {
    @Autowired
    private PcAlipayClient pcAlipayClient;

    @Override
    public AlipayTradeQueryResponse alipayTradeQuery(String tradeNo){
        String content = buildBizContent(tradeNo);
        return alipayTradeQueryByBizContent(content);
    }

    private AlipayTradeQueryResponse alipayTradeQueryByBizContent(String content) {
        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        request.setBizContent(content);
        return pcAlipayClient.execute(request);
    }

    private String buildBizContent(String tradeNo){
        Map<String, Object> content = new HashMap<String, Object>();
        content.put(AlipayConsts.FIELD_NAME_TRADE_NO, tradeNo);
        return JsonUtils.toJsonString(content);
    }
}
