package io.pay.modules.app.service.impl;

import com.alipay.api.request.AlipayFundTransToaccountTransferRequest;
import com.alipay.api.response.AlipayFundTransToaccountTransferResponse;
import io.pay.common.consts.AlipayConsts;
import io.pay.common.utils.DateUtils;
import io.pay.common.utils.JsonUtils;
import io.pay.modules.app.ali.PcAlipayClient;
import io.pay.modules.app.model.AlipayFundTransDO;
import io.pay.modules.app.service.AlipayFundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: helloworld
 * @version: 1.0
 * @created: 2018-02-24
 * @description:
 */
@Service
public class AlipayFundServiceImpl implements AlipayFundService {
    @Autowired
    private PcAlipayClient pcAlipayClient;

    @Autowired
    OrderInfoDBServiceImpl orderInfoDBService;

    @Override
    public AlipayFundTransToaccountTransferResponse alipayFundTransToaccount(String outTradeNo,
                                                                             String outBizNo,
                                                                             String payeeType,
                                                                             String payeeAccount,
                                                                             Double amount,
                                                                             String payerShowName,
                                                                             String remark) {
        String bizContent = buildBizContent(outBizNo, payeeType, payeeAccount, amount, payerShowName, remark);
        AlipayFundTransToaccountTransferResponse transferResponse =  alipayFundTransToaccountByBizContent(bizContent);

        //数据库插入操作
        AlipayFundTransDO fundTransDO = new AlipayFundTransDO();
        fundTransDO.setOutBizNo(outBizNo);
        fundTransDO.setOutTradeNo(outTradeNo);
        fundTransDO.setPayeeType(payeeType);
        fundTransDO.setPayeeAccount(payeeAccount);
        fundTransDO.setAmount(amount);
        fundTransDO.setPayerShowName(payerShowName);
        fundTransDO.setRemark(remark);
        fundTransDO.setOrderId(transferResponse.getOrderId());
        fundTransDO.setResponseOutBizNo(transferResponse.getOutBizNo());
        fundTransDO.setPayDate(transferResponse.getPayDate());
        fundTransDO.setCode(transferResponse.getCode());
        fundTransDO.setMsg(transferResponse.getMsg());
        fundTransDO.setSubCode(transferResponse.getSubCode());
        fundTransDO.setSubMsg(transferResponse.getSubMsg());
        fundTransDO.setBody(transferResponse.getBody());
        fundTransDO.setParams(JsonUtils.toJsonString(transferResponse.getParams()));
        fundTransDO.setCtime(System.currentTimeMillis());
        fundTransDO.setUtime(System.currentTimeMillis());
        fundTransDO.setGmtCreate(DateUtils.getCurrentTime());
        orderInfoDBService.insertAlipayFundTransInfo(fundTransDO);

        return transferResponse;
    }

    private AlipayFundTransToaccountTransferResponse alipayFundTransToaccountByBizContent(String content) {
        AlipayFundTransToaccountTransferRequest request = new AlipayFundTransToaccountTransferRequest();
        request.setBizContent(content);
        return pcAlipayClient.execute(request);
    }

    private String buildBizContent(String outBizNo,
                                   String payeeType,
                                   String payeeAccount,
                                   Double amount,
                                   String payerShowName,
                                   String remark) {
        Map<String, Object> content = new HashMap<String, Object>();
        content.put(AlipayConsts.FIELD_NAME_OUT_BIZ_NO, outBizNo);
        content.put(AlipayConsts.FIELD_NAME_PAYEE_TYPE, payeeType);
        content.put(AlipayConsts.FIELD_NAME_PAYEE_ACCOUNT, payeeAccount);
        content.put(AlipayConsts.FIELD_NAME_AMOUNT, String.valueOf(amount));//todo
        content.put(AlipayConsts.FIELD_NAME_PAYER_SHOW_NAME, payerShowName);
        content.put(AlipayConsts.FIELD_NAME_REMARK, remark);
        return JsonUtils.toJsonString(content);
    }
}
