package io.pay.modules.app.service;

import com.alipay.api.response.AlipayTradeOrderSettleResponse;

/**
 * Created by laaaa on 2018/12/16.
 */
public interface AlipayTradeOrderSettleService {
    public AlipayTradeOrderSettleResponse orderSettle(String outRequestNo,
                                                      String outTradeNo,
                                                      String tradeNo,
                                                      String transOut,
                                                      String transIn,
                                                      Double amount,
                                                      Integer amountPercentage,
                                                      String desc,
                                                      String appAuthToken);
}
