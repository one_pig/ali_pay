package io.pay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.pay.modules.app.model.AlipayFundTransDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created by laaaa on 2018/12/16.
 */
@Mapper
public interface IAlipayFundTransDao extends BaseMapper {
    void insert(AlipayFundTransDO alipayFundTransDO);

    List<AlipayFundTransDO> selectAll();
}
