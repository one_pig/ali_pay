package io.pay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.pay.modules.app.model.PreOrderInfoDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by laaaa on 2018/12/16.
 */
@Mapper
public interface IPreOrderInfoDao extends BaseMapper {
    void insert(PreOrderInfoDO preOrderInfoDO);
    PreOrderInfoDO selectByOutTradeNo(@Param("outTradeNo")String outTradeNo);

    List<PreOrderInfoDO> selectAll();

    void updateQrCode(PreOrderInfoDO preOrderInfoDO);
}
