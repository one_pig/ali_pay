package io.pay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.pay.modules.app.model.AlipayTradeOrderSettleDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Created by laaaa on 2018/12/16.
 */
@Mapper
public interface IAlipayTradeOrderSettleDao extends BaseMapper {
    void insert(AlipayTradeOrderSettleDO alipayTradeOrderSettleDO);

    List<AlipayTradeOrderSettleDO> selectAll();
}
