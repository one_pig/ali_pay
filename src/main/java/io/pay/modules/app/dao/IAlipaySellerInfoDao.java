package io.pay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.pay.modules.app.model.AlipaySellerInfoDO;
import io.pay.modules.app.model.AlipaySellerInfoVO;
import io.pay.modules.app.model.AlipaySellerTradeInfoDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by laaaa on 2018/12/16.
 */
@Mapper
public interface IAlipaySellerInfoDao extends BaseMapper {

    void insert(AlipaySellerInfoDO alipaySellerInfoDO);

    List<AlipaySellerInfoDO> selectAll();

    List<AlipaySellerInfoVO> selectAllUsers();

    void update(AlipaySellerInfoDO alipaySellerInfoDO);

    void delete(String userId);

    AlipaySellerInfoDO selectByUserId(@Param("userId")String userId);

    /**
     * 查询大于等于gmtCreate的码主交易信息,按照日剩余可用额度降序(前200条)
     * @param gmtCreate 2018-04-15 00:00:00
     * @return
     */
    List<AlipaySellerTradeInfoDO> getSellerTradeInfos(@Param("gmtCreate")String gmtCreate);
}
