package io.pay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.pay.modules.app.model.AlipaySellerUserInfoDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by laaaa on 2018/12/16.
 */
@Mapper
public interface IAlipaySellerUserInfoDao extends BaseMapper {
    void insert(AlipaySellerUserInfoDO alipaySellerUserInfoDO);

    void delete(String userId);
}
