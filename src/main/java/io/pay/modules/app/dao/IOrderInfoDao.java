package io.pay.modules.app.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import io.pay.modules.app.model.OrderInfoDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by laaaa on 2018/12/16.
 */
@Mapper
public interface IOrderInfoDao extends BaseMapper {
    void insert(OrderInfoDO orderInfoDO);

    OrderInfoDO selectByOutTradeNo(String outTradeNo);

    List<OrderInfoDO> selectAll();

    List<String> selectByUserId(@Param("buyerId")String buyerId, @Param("gmtCreate")String gmtCreate);
}
